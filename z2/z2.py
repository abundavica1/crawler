from urllib.request import urlopen, Request
from bs4 import BeautifulSoup as soup
import json
import hashlib
import dateutil.parser as dparser
import os
import datetime
import re
from pathlib import Path

#url we want to scrap
myUrl = 'https://www.ibtimes.com'
#categories we want to scrap data from
tabs = ["world", "business"]
#relative path to working directory
dirname = os.path.dirname(__file__)
filename = os.path.join(dirname, 'output/')
Path(filename).mkdir(parents=True, exist_ok=True)
#function for saving data in .json and .txt file
def saveData(data, url, content):
    out_filename = filename + hashlib.md5(url.encode()).hexdigest() #hash url link and name file like that
    #json
    with open(out_filename + '.json', 'w', encoding = 'utf-8') as f:
        json.dump(data, f, ensure_ascii=False, indent=2)
    #txt
    txtFile = open(out_filename + '.txt', 'w')
    txtFile.write(content)
    txtFile.close();


def getPageHtml(url):
    uClient = urlopen(url)
    pageHtml = uClient.read()
    uClient.close()
    return soup(pageHtml, "html.parser")


def getArticleBody(page_html):
    text = ""
    for pContent in page_html:
        part = pContent.find(text=True, recursive=False)
        if part is not None:
            text += part
    return text


def scrapFromCategory(tab, pages):
    for num in range(0, pages):
        page_soup =  getPageHtml(myUrl + "/" + tab + "?page=" + str(num))
        #find feature with articles' links
        features = page_soup.find("div",{"id": re.compile('sticky[1-2]')}).find_all("div",{"class": re.compile('feature|feature2')})
        articles = []

        [articles.extend(feature.find_all("h3")) for feature in features]

        for page_article in articles:

            article_url = page_article.a["href"]
            page_soup2 = getPageHtml(myUrl + article_url)

            try:
                #get published date but exclude news from today
                published_at = dparser.parse(page_soup2.find("meta", {"property":"article:published_time"})["content"], fuzzy=True).strftime("%Y-%m-%dT%H:%M:%S")
                if dparser.parse(published_at).strftime("%Y-%m-%d") == datetime.date.today().strftime('%Y-%m-%d'):
                    continue
                
                #get article title
                title = page_soup2.find("meta", {"property":"og:title"})["content"]
                
                #get publisher, if there is no publisher name, name it Internation Business Times
                publisher = page_soup2.find("meta", {"property":"article:publisher"})
                if len(publisher) == 0:
                    publisher = 'International Business Times'
                               
                data = {'publisher': publisher, 'title': title, 'url': myUrl + article_url, 'published_at': published_at}
                saveData(data, article_url, getArticleBody(page_soup2.find("div", {"class":"article-body"}).find_all("p")))
            
            except:
                print(article_url)
                
#scrap from every mentioned tab
[scrapFromCategory(tab, 3) for tab in tabs]