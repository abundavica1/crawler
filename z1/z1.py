from urllib.request import urlopen, Request
from bs4 import BeautifulSoup as soup
import json
import hashlib
import dateutil.parser as dparser
import os
from pathlib import Path

#data location
dirname = os.path.dirname(__file__)
link = os.path.join(dirname, 'output/')
Path(link).mkdir(parents=True, exist_ok=True)
#link = 'C:/Users/aldina.bundavica/Desktop/scraping/z1/'
#urls we want to scrap
urls = ['http://www.chinadaily.com.cn','http://www.chinadaily.com.cn/china','http://www.chinadaily.com.cn/business/money']

#read all words you want to remove from title and save in list
list = []
with open(dirname + '/data.txt') as f:
    list=f.read().splitlines()

#remove all words in list frome title
def clearTitle(articleTitle):
    for l in list:
        newTitle = str.replace(articleTitle, l, "")
        articleTitle = newTitle
    return articleTitle

#save data from news web page
def saveData(data, url, content):
    out_filename = link + hashlib.md5(url.encode()).hexdigest() #hash url link and name file like that
    with open(out_filename + '.json', 'w', encoding = 'utf-8') as f:
        json.dump(data, f, ensure_ascii=False, indent=4)
    txtFile = open(out_filename + '.txt', 'w')
    txtFile.write(content)
    txtFile.close();

for myUrl in urls:
    uClient = urlopen(myUrl)
    pageHtml = uClient.read()
    uClient.close()
    page_soup =  soup(pageHtml, "html.parser")
    containers = page_soup.findAll("a", {"target":"_blank"})

    for container in containers:
        article_url = container["href"]
        if article_url.find('http') == -1 :
            article_url = 'http:' + container["href"]
        
        req = Request(article_url, headers={'User-Agent': 'Mozilla/5.0'}) 
        #try scrap in case page is not forbidden (error 403)
        try:
            pageHtml = urlopen(article_url).read()
            article_soup = soup(pageHtml, "html.parser")
            #try if page has news
            try: 
                title = clearTitle(article_soup.title.string)

                #get published date and publisher from info
                info = article_soup.find("div", {"class":"info"}).span

                #publisher should always be 'China Daily Information Co', if it's not, don't save news
                if info.find("chinadaily.com.cn") == -1:
                    continue
                publisher = 'China Daily Information Co'

                #published date from info in iso 8601 format
                publishedAt = dparser.parse(info, fuzzy=True).isoformat()

                #get news text, exclude last chapter, images, links, references...
                text = ""
                text_parts = article_soup.find("div", {"id":"Content"}).find_all('p')
                text_parts.pop()
                for el in text_parts:
                    text += el.string
                
                #save data
                data = {'publisher': publisher, 'title': title, 'url': article_url, 'publishedAt': publishedAt}
                saveData(data, article_url, text)
            except:
                print(article_url + ' nema podataka')
        except:
            print(article_url + ' 403')
