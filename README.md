# Crawler

**z1.py**
_web scraping from urls:
http://www.chinadaily.com.cn/ 
http://www.chinadaily.com.cn/china
http://www.chinadaily.com.cn/business/money_

Words that are needed to be removed from title are written in z1/data.txt

Created output files are located in z1/output/md5Url
.json for meta data
.txt for artible body

Folder output is not empty but keeps some data as a proof of code working fine. 
_____________________________________________________

**z2.py**
_web scraping from urls:
_https://www.ibtimes.com/business
https://www.ibtimes.com/world
from pages 1-3_

Articles in columns like _Most read, Picture Galleries, Latest News_ etc. are not collected but only articles shown on specific page.

Created output files are located in z2/output/md5Url
.json for meta data
.txt for artible body

Folder output is not empty but keeps some data as a proof of code working fine. 

